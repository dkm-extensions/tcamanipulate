<?php

if (!defined ('TYPO3')) {
    die ('Access denied.');
}
//$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tceforms.php']['getMainFieldsClass'][]='EXT:tcamanipulate/class.tcamanipulate_tceforms_procTCAtitle.php:user_tcamanipulate_tceforms_procTCAtitle';

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['typo3/class.db_list_extra.inc']['getTable']['tcamanipulate'] = \DKM\TCAManipulate\Hooks\WebListAddFields::class;
if ((new \TYPO3\CMS\Core\Information\Typo3Version())->getMajorVersion() >= 11) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS'][\TYPO3\CMS\Backend\RecordList\DatabaseRecordList::class]['modifyQuery']['tcamanipulate'] = \DKM\TCAManipulate\Hooks\WebListAddFields::class;
}
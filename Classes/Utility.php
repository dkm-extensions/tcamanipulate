<?php

namespace DKM\TCAManipulate;

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Utility
{
    /**
     * @return int|mixed|string|null
     */
    public static function getPageId(): mixed
    {
        if(!isset($GLOBALS['TYPO3_REQUEST'])) return null;
        $pageId = $GLOBALS['TYPO3_REQUEST']->getParsedBody()['id'] ?? $GLOBALS['TYPO3_REQUEST']->getQueryParams()['id'] ?? null;
        if (!$pageId) {
            $editConf = $GLOBALS['TYPO3_REQUEST']->getParsedBody()['edit'] ?? $GLOBALS['TYPO3_REQUEST']->getQueryParams()['edit'] ?? null;
            //IF TCEFORM
            if (is_array($editConf)) {
                foreach ($editConf as $table => $uidValue) {
                    if (is_array($uidValue)) {
                        if (current($uidValue) == 'new' && key($uidValue) > 0 || $table == 'pages') {
                            $pageId = key($uidValue);
                        } elseif(is_numeric(key($uidValue))) {
                            $recordArr = \TYPO3\CMS\Backend\Utility\BackendUtility::getRecord($table, intval(abs(key($uidValue))), 'pid');
                            $pageId = $recordArr['pid'] ?? null;
                        }
                    }
                }
            } else {
                //IF IRRE
                $ajax = $GLOBALS['TYPO3_REQUEST']->getParsedBody()['ajax'] ?? $GLOBALS['TYPO3_REQUEST']->getQueryParams()['ajax'] ?? null;
                if (is_array($ajax)) {
                    preg_match('/^data-(\d+)/', $ajax[0], $matches);
                    $pageId = $matches[1] ?? 0;
                }
            }
        }
        return $pageId;
    }

    /**
     * @param int $pageId
     */
    public static function manipulate(int $pageId): void
    {
        if (intval($pageId) !== 0) {
            $modTSConfig = \TYPO3\CMS\Backend\Utility\BackendUtility::getPagesTSconfig($pageId); //Gets the TSconfig
            if (is_array($modTSConfig) && isset($modTSConfig['TCA.']) && is_array($modTSConfig['TCA.'])) {
                $newTCA = GeneralUtility::removeDotsFromTS($modTSConfig['TCA.']);
                unset($modTSConfig['includeLibs'], $modTSConfig['renameFields']);
                ArrayUtility::mergeRecursiveWithOverrule($GLOBALS['TCA'], (is_array($newTCA) ? $newTCA : []));
            }
        }
    }

}
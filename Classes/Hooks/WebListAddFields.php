<?php


namespace DKM\TCAManipulate\Hooks;

use TYPO3\CMS\Backend\RecordList\RecordListGetTableHookInterface;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Http\ServerRequest;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Recordlist\RecordList\DatabaseRecordList;

class WebListAddFields implements RecordListGetTableHookInterface, SingletonInterface
{
    /**
     * @var Site
     */
    protected $site;

    /**
     * @var \TYPO3\CMS\Backend\RecordList\DatabaseRecordList
     */
    protected $parentObj;

    /**
     * Current table
     * @var string
     */
    protected $table;

    protected array $fieldsToAdd = [];

    /**
     * @param string $table
     * @param int $pageId
     * @param string $additionalWhereClause
     * @param string $selectedFieldsList
     * @param \TYPO3\CMS\Backend\RecordList\DatabaseRecordList $parentObject
     */
    public function getDBlistQuery($table, $pageId, &$additionalWhereClause, &$selectedFieldsList, &$parentObject)
    {
        $this->parentObj = &$parentObject;
        $this->table = $table;
        /** @var ServerRequest $request */
        $request = $GLOBALS['TYPO3_REQUEST'];
        $this->site = $request->getAttribute('site');
        $this->disableColumnInListModule('disableLocalizationInListModule', ['_LOCALIZATION_',
            '_LOCALIZATION_b']);
        $this->disableColumnInListModule('disableDescriptionColumnInListModule', [$GLOBALS['TCA'][$table]['ctrl']['descriptionColumn'] ?? '']);
        $this->addColumnsToListModule($selectedFieldsList, $table);
    }

    /**
     * @param $selectedFieldsList
     */
    protected function addColumnsToListModule(&$selectedFieldsList, $table)
    {
        if($fieldsToAdd = $this->getFieldsToAdd($table)) {
            $selectedFieldsList .= ',' . implode(',', $fieldsToAdd);
            $currentFields = $this->parentObj->setFields[$table] ?? [];
            array_splice($currentFields, 1, 0, $fieldsToAdd);
            $this->parentObj->fieldArray = array_unique(array_merge($this->parentObj->fieldArray, $fieldsToAdd));
            //disabled this
//            $this->parentObject->setFields[$table] = array_unique($currentFields);
        }


    }

    /**
     * @param $fromTable
     */
    protected function getFieldsToAdd($fromTable): array
    {
        if(!$this->fieldsToAdd && ($this->site && get_class($this->site) !== 'TYPO3\CMS\Core\Site\Entity\NullSite' && get_class($this->site) !== 'TYPO3\CMS\Core\Site\Entity\PseudoSite' && ($addColumnsToListModuleCfg = $this->site->getConfiguration()['TCAManipulate']['addColumnsToListModule'] ?? false))) {
            foreach ($addColumnsToListModuleCfg as $table => $config) {
                if ($table === $this->table) {
                    if (($config['pagesModuleFieldEquals'] ?? false) && $config['pagesModuleFieldEquals'] !== $this->parentObj->pageRow['module']) {
                        continue;
                    }
                    $this->fieldsToAdd[$table] = array_reverse($config['fieldsToAdd']);
                }
            }
        }
        return $this->fieldsToAdd[$fromTable] ?? [];
    }

    /**
     * @param string $configKey
     * @param array $values
     * @param string $table
     */
    protected function disableColumnInListModule($configKey, $values)
    {
        $disable = false;
        if (get_class($this->site) !== 'TYPO3\CMS\Core\Site\Entity\NullSite' && get_class($this->site) !== 'TYPO3\CMS\Core\Site\Entity\PseudoSite' && ($this->site->getConfiguration()['TCAManipulate'][$configKey] ?? false)) {
            if (is_array($this->site->getConfiguration()['TCAManipulate'][$configKey])) {
                if (in_array($this->table, $this->site->getConfiguration()['TCAManipulate'][$configKey])) {
                    $disable = true;
                }
            } elseif ($this->site->getConfiguration()['TCAManipulate'][$configKey] == '*' || $this->site->getConfiguration()['TCAManipulate'][$configKey]) {
                $disable = true;
            }
        }
        if ($disable) {
            $this->unsetArrayElementByValue($this->parentObj, $values);
        }
    }

    /**
     * @param $parentObject
     * @param array $value
     */
    protected function unsetArrayElementByValue(&$parentObject, $values)
    {
        foreach ($values as $value) {
            $key = $value ? array_search($value, $parentObject->fieldArray) : false;
            if ($key !== false) {
                unset($parentObject->fieldArray[$key]);
            }
        }
    }

    /**
     * @param $parameters
     * @param $table
     * @param $pageId
     * @param $additionalConstraints
     * @param $fields
     */
    public function modifyQuery($parameters, $table, $pageId, $additionalConstraints, $fields, QueryBuilder $queryBuilder): void
    {
        if($fieldsToAdd = $this->getFieldsToAdd($table)) {
            $queryBuilder->addSelect(...$fieldsToAdd);
        }
    }
}
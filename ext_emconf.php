<?php

########################################################################
# Extension Manager/Repository config file for ext "tcamanipulate".
#
# Auto generated 04-06-2012 13:25
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'TCA manipulation via PageTS',
	'description' => 'With this extension you can manipulate the TCA on page basis via PageTS.',
	'category' => 'be',
	'version' => '1.3.0',
	'state' => 'beta',
	'clearcacheonload' => 0,
	'author' => 'Stig Nørgaard Færch',
	'author_email' => 'snf@dkm.dk',
	'author_company' => '',
	'constraints' => array(
		'depends' => array(
			'typo3' => '8.7.0-11.5.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);

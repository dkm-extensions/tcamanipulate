<?php
/**
 * An array consisting of implementations of middlewares for a middleware stack to be registered
 *
 *  'stackname' => [
 *      'middleware-identifier' => [
 *         'target' => classname or callable
 *         'before/after' => array of dependencies
 *      ]
 *   ]
 */
return [
    'backend' => [
        /** internal: do not use or reference this middleware in your own code */
        'dkm/tcamanipulate' => [
            'target' => \DKM\TCAManipulate\Middleware\TCAManipulation::class,
            'after' => [
                'typo3/cms-backend/authentication'
            ]
        ],
    ]
];

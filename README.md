## Configuration example

### List module manipulation
Site configuration:
```TCAManipulate:
  disableLocalizationInListModule: true
  disableDescriptionColumnInListModule: true
  addColumnsToListModule:
    tx_news_domain_model_news:
      pagesModuleFieldEquals: 'eventnewsplugin'
      fieldsToAdd:
        - 'event_end'
        - 'datetime'
```